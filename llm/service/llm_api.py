# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------

# Date    : 2023/10/12 23:42
# File    : models/llm/service/llm_api.py
# Desc    : ChatGLM2-6B+LightLLM的API接口服务
# Author  : Damon Li 李自然
# E-mail  : bingzhenli@hotmail.com / liziran1@richinfo.cn
# CLI     : python llm_api.py --config_dir 'configs/llm_server_config.json'

# ------------------------------------------------------------------------------

import os
import sys
import uuid
import json

import datetime
import argparse

from typing import AsyncGenerator, List

import uvicorn
import socket
import asyncio
from http import HTTPStatus
import multiprocessing as mp

from fastapi import FastAPI, Request, BackgroundTasks
from fastapi.responses import Response, JSONResponse, StreamingResponse

import torch
from transformers import AutoTokenizer

from lightllm.sampling_params import SamplingParams
from lightllm.server.httpserver.manager import HttpServerManager
from lightllm.server.router.manager import start_router_process
from lightllm.server.detokenization.manager import start_detokenization_process

from utils import load_model_on_gpus, dict2obj
from logger import init_logger


parser = argparse.ArgumentParser()
parser.add_argument('--config_dir', type=str, required=True, help='JSON配置路径')
args = parser.parse_args()

service_configs = json.load(open(args.config_dir, 'r', encoding='utf-8'))
service_configs = dict2obj(service_configs)

assert hasattr(service_configs, 'host')
assert hasattr(service_configs, 'port')
assert hasattr(service_configs, 'model_name_or_path')
assert hasattr(service_configs, 'num_gpus')

logger = init_logger()
logger.debug(service_configs)

DEVICE = service_configs.device
DEVICE_ID = service_configs.device_id
CUDA_DEVICE = f'{DEVICE}:{DEVICE_ID}' if DEVICE_ID else DEVICE
TIMEOUT_KEEP_ALIVE = 5  # seconds.

app = FastAPI()
model = None
tokenizer = None
isFirst = True


def error_response(status_code: HTTPStatus, message: str) -> JSONResponse:
    return JSONResponse({"message": message}, status_code=status_code.value)


def torch_gc():
    if torch.cuda.is_available():
        with torch.cuda.device(CUDA_DEVICE):
            torch.cuda.empty_cache()
            torch.cuda.ipc_collect()


def load_model_without_lightllm():
    global service_configs, model, tokenizer
    tokenizer = AutoTokenizer.from_pretrained(service_configs.model_name_or_path, trust_remote_code=True)
    model = load_model_on_gpus(service_configs.model_name_or_path, num_gpus=service_configs.num_gpus)
    model = model.eval()


def alloc_can_use_network_port(num=3, used_nccl_port=None):
    port_list = []
    for port in range(10000, 65536):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            result = s.connect_ex(("localhost", port))
            if result != 0 and port != used_nccl_port:
                port_list.append(port)
            if len(port_list) == num:
                return port_list
    return None


def load_model_with_lightllm():
    global service_configs, httpserver_manager

    args = service_configs.LightLLM
    assert args.max_req_input_len < args.max_req_total_len

    if args.batch_max_tokens is None:
        batch_max_tokens = int(1 / 6 * args.max_total_token_num)
        batch_max_tokens = max(batch_max_tokens, args.max_req_total_len)
        args.batch_max_tokens = batch_max_tokens
    else:
        assert (
                args.batch_max_tokens >= args.max_req_total_len
        ), "batch_max_tokens must >= max_req_total_len"

    can_use_ports = alloc_can_use_network_port(
        num=3 + args.tp, used_nccl_port=args.nccl_port
    )

    router_port, detokenization_port, httpserver_port = can_use_ports[0:3]
    model_rpc_ports = can_use_ports[3:]

    httpserver_manager = HttpServerManager(
        args.model_dir,
        args.tokenizer_mode,
        router_port=router_port,
        httpserver_port=httpserver_port,
        total_token_num=args.max_total_token_num,
        max_req_input_len=args.max_req_input_len,
        max_req_total_len=args.max_req_total_len,
        trust_remote_code=args.trust_remote_code,
    )
    pipe_router_reader, pipe_router_writer = mp.Pipe(duplex=False)
    pipe_detoken_reader, pipe_detoken_writer = mp.Pipe(duplex=False)
    proc_router = mp.Process(
        target=start_router_process,
        args=(
            args,
            router_port,
            detokenization_port,
            model_rpc_ports,
            args.mode,
            pipe_router_writer,
        ),
    )
    proc_router.start()
    proc_detoken = mp.Process(
        target=start_detokenization_process,
        args=(
            args,
            detokenization_port,
            httpserver_port,
            pipe_detoken_writer,
            args.trust_remote_code,
        ),
    )
    proc_detoken.start()

    # wait load model ready
    router_init_state = pipe_router_reader.recv()
    detoken_init_state = pipe_detoken_reader.recv()

    if router_init_state != "init ok" or detoken_init_state != "init ok":
        proc_router.kill()
        proc_detoken.kill()
        print(
            "router init state:",
            router_init_state,
            "detoken init state:",
            detoken_init_state,
        )
        sys.exit(1)

    assert proc_router.is_alive() and proc_detoken.is_alive()


def build_prompt(query, history=None):
    if history is None:
        history = []
    prompt = ""
    for i, (old_query, response) in enumerate(history):
        prompt += "[Round {}]\n\n问：{}\n\n答：{}\n\n".format(i + 1, old_query, response)
    prompt += "[Round {}]\n\n问：{}\n\n答：".format(len(history) + 1, query)

    return prompt


@app.get('/aibox/chatLLM/v1.0/getConfigDesc')
def get_service_configs_desc():
    global service_configs

    _out = {
        "host": "URL地址，默认0.0.0.0",
        "port": "端口号，默认8000",
        "model_name_or_path": "模型名称或具体路径",
        "num_gpus": "GPU并行推理数，默认为1"
    }
    logger.debug("!!!!"+str(service_configs))

    if hasattr(service_configs, 'LightLLM'):
        logger.debug(111)
        _out.update({
            "LightLLM": {
                "tokenizer_mode": "加载分词器的模式，可以是慢速或自动，慢速模式加载快但运行慢，慢速模式适合调试和测试，当你想获得最佳性能时，尝试auto模式。",
                "max_total_token_num": "GPU和模型可以支持的总token数量，等于 = 最大批次 * (输入长度 + 输出长度)，默认6000",
                "batch_max_tokens": "新Batch的最大token数量，它控制预填充批次大小以防止内存溢出（OOM），默认是None",
                "eos_id": "结束符eos的token id，默认是2",
                "running_max_req_size": "同时进行的前向推理请求的最大数量，默认是1000",
                "tp": "模型并行大小，默认值是1",
                "max_req_input_len": "请求输入token数量的最大值，默认值是2048",
                "max_req_total_len": "请求输入长度加上请求输出长度的最大值，默认值是3072",
                "nccl_port": "用于为PyTorch构建分布式环境的nccl_port，默认值是28765",
                "mode": "模型模式，默认是[]，还包括[int8kv]和[int8weight | int4weight]；"
                        "[int8kv]是键值对（kv）都以8位整数（int8）表示。"
                        "[int8weight | int4weight]：是两种模式的选择，一种是权重以8位整数（int8weight）表示，另一种是权重以4位整数（int4weight）表示。",
                "trust_remote_code": "是否允许在Hub上定义自己的建模文件中的自定义模型，action='store_true'",
                "disable_log_stats": "禁用吞吐量统计的记录，action='store_true'",
                "log_stats_interval": "间隔多少秒统计一次日志，默认是1秒"
            }
        })
    logger.debug(_out)

    return JSONResponse({"param_desc_in_chn": _out}, status_code=HTTPStatus.OK.value)


@app.post('/aibox/chatLLM/v1.0/generate')
async def generate(request: Request):
    json_post_raw = await request.json()

    json_post = json.dumps(json_post_raw)
    json_post_dict = json.loads(json_post)

    global service_configs
    if hasattr(service_configs, 'LightLLM'):
        global isFirst
        if isFirst:
            loop = asyncio.get_event_loop()
            loop.create_task(httpserver_manager.handle_loop())
            isFirst = False

        prompt = json_post_dict.pop('prompt')
        history: List[List] = json_post_dict.pop('history', [])
        if len(history) != 0:
            prompt = build_prompt(prompt, history)

        max_length = json_post_dict.pop('max_length', 32000)
        return_details = json_post_dict.pop("return_details", False)

        logger.info("post payload -> \n{}".format(str(json_post_dict)))

        sampling_params = SamplingParams(**json_post_dict)
        sampling_params.verify()

        request_id = uuid.uuid4().hex
        results_generator = httpserver_manager.generate(prompt, sampling_params, request_id)

        # Non-streaming case
        final_output = []
        count_output_tokens = 0
        tokens = []
        # _id = 0
        async for request_output, metadata, _ in results_generator:
            count_output_tokens += 1
            if await request.is_disconnected():
                # Abort the request if the client disconnects.
                await httpserver_manager.abort(request_id)
                now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                answer = {
                    'response': '',
                    'history': [],
                    'status': 499,
                    'time': now
                }
                logger.error('api_lightllm.py::Abort the request since the client disconnects')
                return answer
                # return Response(status_code=499)

            # print('id={}req_out_str = {}'.format(_id, request_output))
            # _id += 1

            final_output.append(request_output)
            if return_details:
                metadata["text"] = request_output
                tokens.append(metadata)

        assert final_output is not None

        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        answer = {
            'response': "".join(final_output),
            'history': [],
            'status': 200,
            'time': now
        }
        logger.debug('prompt: {0} \nresponse: {1}'.format(now, prompt, repr(answer['response'])))

        if return_details:
            answer["tokens"] = tokens

    else:
        prompt = json_post_dict.get('prompt')
        history = json_post_dict.get('history')
        max_length = json_post_dict.get('max_length')
        top_p = json_post_dict.get('top_p')
        temperature = json_post_dict.get('temperature')

        response, history = model.chat(tokenizer,
                                       prompt,
                                       history=history,
                                       max_length=max_length if max_length else 2048,
                                       top_p=top_p if top_p else 0.7,
                                       temperature=temperature if temperature else 0.95)

        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        answer = {
            'response': response,
            'history': history,
            'status': 200,
            'time': now
        }
        logger.debug('prompt: {0} \nresponse: {1}'.format(now, prompt, repr(response)))

    torch_gc()

    return answer


@app.post("/aibox/chatLLM/v1.0/generate_stream")
async def generate_stream(request: Request) -> Response:
    json_post_raw = await request.json()

    json_post = json.dumps(json_post_raw)
    json_post_dict = json.loads(json_post)

    global service_configs
    assert hasattr(service_configs, 'LightLLM')

    global isFirst
    if isFirst:
        loop = asyncio.get_event_loop()
        loop.create_task(httpserver_manager.handle_loop())
        isFirst = False

    prompt = json_post_dict.pop('prompt')
    history = json_post_dict.pop('history', [])
    prompt = build_prompt(prompt, history)
    max_length = json_post_dict.pop('max_length', 32000)
    return_details = json_post_dict.pop("return_details", False)

    logger.info("post payload from RASA Action Server -> \n{}".format(str(json_post_dict)))

    sampling_params = SamplingParams(**json_post_dict)
    sampling_params.verify()

    request_id = uuid.uuid4().hex
    results_generator = httpserver_manager.generate(prompt, sampling_params, request_id)

    # Streaming case
    async def stream_results() -> AsyncGenerator[bytes, None]:
        async for request_output, metadata, finished in results_generator:
            ret = {
                "token": {
                    "id": metadata.get("id", None),
                    "text": request_output,
                    "logprob": metadata.get("logprob", None),
                },
                "finished": finished,
            }

            yield ("data:" + json.dumps(ret, ensure_ascii=False) + f"\n\n").encode(
                "utf-8"
            )

    async def abort_request() -> None:
        await httpserver_manager.abort(request_id)

    background_tasks = BackgroundTasks()
    # Abort the request if the client disconnects.
    background_tasks.add_task(abort_request)

    return StreamingResponse(
        stream_results(), media_type="text/event-stream", background=background_tasks
    )


if __name__ == '__main__':
    logger.debug('加载本地大模型路径：{}'.format(service_configs.model_name_or_path))

    if not hasattr(service_configs, 'LightLLM'):
        load_model_without_lightllm()
    else:
        os.environ['nccl_port'] = str(service_configs.LightLLM.nccl_port)
        load_model_with_lightllm()

    uvicorn.run(app, host=service_configs.host, port=service_configs.port,
                workers=1)  # v1

