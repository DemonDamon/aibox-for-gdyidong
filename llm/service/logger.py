# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------

# Date    : 2023/10/13 20:10
# File    : llm/service/logger.py
# Desc    : 
# Author  : Damon Li 李自然
# E-mail  : bingzhenli@hotmail.com / liziran1@richinfo.cn

# ------------------------------------------------------------------------------

import sys

from types import FrameType
from typing import cast

import logging
from loguru import logger


loguru_config = {
    "handlers": [
        {
            "sink": sys.stdout,
            "level": logging.DEBUG,
            "format": "<green>{time:YYYY-mm-dd HH:mm:ss.SSS}</green> "
                      "| {thread.name} "
                      "| <level>{level}</level> | "
                      "<cyan>{module}</cyan>:"
                      "<cyan>{function}</cyan>:"
                      "<cyan>{line}</cyan> - "
                      "<level>{message}</level>"
        },
        {
            "sink": "service/logs/llm.log",
            "level": logging.DEBUG,
            "rotation": "10 MB",
            "retention": "1 week",
            "encoding": 'utf-8',
            "format": "{time:YYYY-mm-dd HH:mm:ss.SSS} "
                      "| {thread.name} "
                      "| {level} "
                      "| {module} : {function}:{line} - "
                      "{message}"
        },
        {
            "sink": "service/logs/llm_error.log",
            "serialize": True,
            "level": 'ERROR',
            "retention": "1 week",
            "rotation": "10 MB",
            "encoding": 'utf-8',
            "format": "{time:YYYY-mm-dd HH:mm:ss.SSS} "
                      "| {thread.name} "
                      "| {level} "
                      "| {module} : {function}:{line} - "
                      "{message}"
        },
    ],
}


class InterceptHandler(logging.Handler):
    def emit(self, record: logging.LogRecord) -> None:  # pragma: no cover
        # Get corresponding Loguru level if it exists
        try:
            level = logger.level(record.levelname).name
        except ValueError:
            level = str(record.levelno)

        # Find caller from where originated the logged message
        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:  # noqa: WPS609
            frame = cast(FrameType, frame.f_back)
            depth += 1

        logger.opt(depth=depth, exception=record.exc_info).log(
            level, record.getMessage(),
        )


def init_logger():
    LOGGER_NAMES = ("uvicorn", "uvicorn.access",)
    for logger_name in LOGGER_NAMES:
        logging_logger = logging.getLogger(logger_name)
        logging_logger.handlers = [InterceptHandler()]
    logger.configure(**loguru_config)

    return logger
