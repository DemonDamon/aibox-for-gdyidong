# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------

# ██╗   ██╗██╗   ██╗███╗   ██╗ ██╗██████╗  █████╗ 
# ╚██╗ ██╔╝██║   ██║████╗  ██║███║╚════██╗██╔══██╗
#  ╚████╔╝ ██║   ██║██╔██╗ ██║╚██║ █████╔╝╚██████║
#   ╚██╔╝  ██║   ██║██║╚██╗██║ ██║ ╚═══██╗ ╚═══██║
#    ██║   ╚██████╔╝██║ ╚████║ ██║██████╔╝ █████╔╝
#    ╚═╝    ╚═════╝ ╚═╝  ╚═══╝ ╚═╝╚═════╝  ╚════╝ 

# Date    : 2023/12/10 11:25
# File    : AiboxForYun139/models/llm/tests/press_test.py
# Desc    : LLM压力测试
# Author  : Damon Li
# E-mail  : bingzhenli@hotmail.com

# ------------------------------------------------------------------------------


import time
import requests
from concurrent.futures import ThreadPoolExecutor

# 假设这是你的流式推理接口
endpoint = "/aibox/chatLLM/v1.0/generate_stream"
STREAMING_INFERENCE_URL = f'http://192.168.32.115:8000{endpoint}'

# 压测配置
CONCURRENT_REQUESTS = 10  # 同时并发请求的数量
TOTAL_REQUESTS = 100  # 总请求次数
TOKENS_PER_REQUEST = 15  # 假设每个请求处理的Token数量
"""
Total time: 35.89 seconds

QPS (Queries per second): 2.79

Average time per request: 3.57 seconds

Tokens per second: 41.79
"""


def make_request(session, url):
    """发送单个请求到推理接口，并返回处理时间"""
    start_time = time.time()
    _payload = {
        "prompt": "你是谁，你能干什么",
        "history": [
            [
                "你是RichAI彩讯智能助手🤖，可以作为您的邮件秘书：\n1. 我能够帮您处理一些复杂的操作：目前支持“打包发票的指令”，例如输入“帮我把这月收到的发票附件打包发送到我的邮箱”，即可触发指令完成打包发票的动作。未来，我将支持更多的复杂指令；\n2. 我可以生成您想要的文本内容：如写邮件、写会议纪要等，如果给我一些提示信息我能生成得更加准确；\n3. 我还会很多常识问题，您可以尝试问我一下。",
                "好的"
            ],
            [
                "注意：目前仅支持以下邮箱场景的复杂指令，其他指令暂不支持!",
                "好的"
            ]
        ],
        "do_sample": False,
        "presence_penalty": 0.0,
        "frequency_penalty": 0.0,
        "temperature": 0.8,
        "top_p": 0.8,
        "ignore_eos": False,
        "max_new_tokens": 2048
    }
    response = session.post(url, json=_payload)
    end_time = time.time()
    # 确保请求成功
    assert response.status_code == 200
    return end_time - start_time


def main():
    with requests.Session() as session:
        # 使用ThreadPoolExecutor来管理并发请求
        with ThreadPoolExecutor(max_workers=CONCURRENT_REQUESTS) as executor:
            # 开始计时
            start_time = time.time()
            # 使用线程池发起请求
            futures = [
                executor.submit(make_request, session, STREAMING_INFERENCE_URL)
                for _ in range(TOTAL_REQUESTS)]
            # 等待所有请求完成
            times = [future.result() for future in futures]
            # 结束计时
            end_time = time.time()

    # 计算指标
    total_time = end_time - start_time
    qps = TOTAL_REQUESTS / total_time
    avg_time_per_request = sum(times) / len(times)
    tokens_per_second = (TOKENS_PER_REQUEST * TOTAL_REQUESTS) / total_time

    print(f"Total time: {total_time:.2f} seconds")
    print(f"QPS (Queries per second): {qps:.2f}")
    print(f"Average time per request: {avg_time_per_request:.2f} seconds")
    print(f"Tokens per second: {tokens_per_second:.2f}")


if __name__ == "__main__":
    main()

