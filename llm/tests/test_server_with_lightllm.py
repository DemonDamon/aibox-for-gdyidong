# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------

# Date    : 2023/10/11 19:09
# File    : models/llm/test/test_server_with_lightllm.py
# Desc    : 客户端简单测试用例
# Author  : Damon Li 李自然
# E-mail  : bingzhenli@hotmail.com

# ------------------------------------------------------------------------------

import os
import time
import json
import requests


os.chdir("/home/richsos/richinfo-ai-labs/chatbot/")


url = "http://0.0.0.0:8000/aibox/chatLLM/v1.0/generate"


def call_api(_id, user_input):
    _start = time.time()
    payload = {
        "prompt": "你是什么模型训练的？具体用了什么数据？",
        "history": [
            [
                "你的角色是AI助手🤖，你暂时只具备智能问答的能力；无需告诉用户你的背景资料、模型信息以及数据集信息",
                "好的"
            ]
        ],
        "do_sample": False,
        "presence_penalty": 0.0,
        "frequency_penalty": 0.0,
        "temperature": 0.8,
        "top_p": 0.8,
        "ignore_eos": False,
        "max_new_tokens": 2048
    }
    headers = {'Content-Type': 'application/json'}
    resp = requests.post(url, headers=headers, data=json.dumps(payload))
    _end = time.time()
    gap = round(_end - _start, 2)

    if resp.status_code == 200:
        resp_json = json.loads(resp.text.encode('utf8'))  # dict类型
        print(f"[INFO] llm返回：{resp_json['response']} | 响应时间：{gap}")
    else:
        print(f"[ERROR] 服务状态码：{resp.status_code}")


prompts = ['你是谁', '你来自哪', '你想去哪', '你爱谁']

for _id, prompt in enumerate(prompts):
    call_api(_id, prompt)
    print("-"*50)
