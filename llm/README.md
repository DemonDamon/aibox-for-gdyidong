# 环境准备
## Conda虚拟环境
> conda create --name py310_llm python=3.10.12

## 安装依赖
1. 先起环境
> conda activate py310_llm

2. 然后安装requirements
> pip install -r requirements.txt

3. 最后再安装LightLLM的依赖，注意`--no-deps`
> pip install triton==2.0.0.dev20221202 --no-deps