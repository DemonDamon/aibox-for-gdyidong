# 查找并杀死服务进程
pid=$(ps -ef | grep "python service/llm_api" | grep -v grep | awk '{print $2}')
if [ -n "$pid" ]; then
    echo "Found <LLM: chatglm2/3-6b> running process with PID: $pid"
    kill $pid
    echo "Service <LLM: chatglm2/3-6b> stopped."
else
    echo "Service <LLM: chatglm2/3-6b> is not running."
fi