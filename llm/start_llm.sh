source activate py310_chatllm

param=$1
nohup=${param#*=}

if [ "$nohup" = "true" ]; then
  echo "<LLM> 服务以nohup启动"
  nohup python service/llm_api.py --config_dir configs/llm_server_config.json > llm_chatglm2.log &
else
  echo "<LLM> 服务默认不以nohup启动，如需nohup启动，启动命令如<sh ./xxx.sh nohup=true>"
  python service/llm_api.py --config_dir configs/llm_server_config.json
fi