# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：main
# @Date   ：2024/1/23 0:36
# @Author ：leemysw

# 2024/1/23 0:36   Create
# =====================================================
from cores.config import settings

import uvicorn
from uvicorn.config import LOGGING_CONFIG

from apps.server.server import create_app
from utils.logger import logger

LOGGING_CONFIG["formatters"]["access"]["datefmt"] = \
    "%Y-%m-%d %H:%M:%S"
LOGGING_CONFIG["formatters"]["access"]["fmt"] = \
    "%(asctime)s -> %(levelname)-8s: %(module)-15s | %(lineno)-3d | %(message)s"

LOGGING_CONFIG["formatters"]["default"]["datefmt"] = \
    "%Y-%m-%d %H:%M:%S"
LOGGING_CONFIG["formatters"]["default"]["fmt"] = \
    "%(asctime)s -> %(levelname)-8s: %(module)-15s | %(lineno)-3d | %(message)s"

app = create_app()

if __name__ == '__main__':
    print(settings.LOGO)
    logger.info('=======================================')
    settings.status(logger=logger)
    logger.info('=======================================')

    uvicorn.run(
        'main:app',
        host=settings.HOST,
        port=settings.PORT,
        reload=True,
        lifespan='on',
        log_config=LOGGING_CONFIG
    )
