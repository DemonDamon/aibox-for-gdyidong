# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：config
# @Date   ：2024/1/22 15:58
# @Author ：leemysw

# 2024/1/22 15:58   Create
# =====================================================
import os
from sys import platform

IS_MAC = platform == "darwin"
IS_WIN = platform == "win32"
IS_LINUX = platform == "linux"
ROOT_PATH = os.path.abspath(os.path.dirname(__file__)) + '/../'

# http://patorjk.com/software/taag/#p=display&f=Lil%20Devil&t=v%201.0.1%0A
logo = """



8888888b.  8888888        d8888  .d8888b.   .d8888b. 88888888888 
888   Y88b   888         d88888 d88P  Y88b d88P  Y88b    888     
888    888   888        d88P888 Y88b.      Y88b.         888     
888   d88P   888       d88P 888  "Y888b.    "Y888b.      888     
8888888P"    888      d88P  888     "Y88b.     "Y88b.    888     
888 T88b     888     d88P   888       "888       "888    888     
888  T88b    888    d8888888888 Y88b  d88P Y88b  d88P    888     
888   T88b 8888888 d88P     888  "Y8888P"   "Y8888P"     888     
                                                                 
                                                                 
                                                                 
        """


class Settings:
    LOGO: str = logo
    HOST: str = "0.0.0.0"
    # PORT: str = 12301
    PORT: str = 8000
    DOMAIN: str = 'http://localhost:8000/'
    DEBUG: bool = True
    SQLALCHEMY_DATABASE_URI: str = "sqlite:///./data.db"
    SQLALCHEMY_TRACK_MODIFICATIONS: bool = False
    PROJECT_NAME: str = "RIAssistant"
    API_V1_STR: str = "/api/v1"
    ALGORITHM = "HS256"

    # set device to use for training and inference
    DEVICE: str = "cpu"

    # set file path for knowledge base
    FILEPATH: str = os.path.join(ROOT_PATH, 'resources/files')

    # SECRET_KEY: str = secrets.token_urlsafe(32)
    # to get a string like this run:
    # openssl rand -hex 32
    SECRET_KEY = "b48291fc83f262fed0629fd3c11cd410538de446866ab2cf8ab70f16c0fecbe9"
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 8
    BACKEND_CORS_ORIGINS = ["*"]

    # set model path
    if IS_MAC:
        DATA_PATH = "/Volumes/data/models"
    elif IS_WIN:
        DATA_PATH = r"/m/models"
    elif IS_LINUX:
        # DATA_PATH = r"/mnt/m/models"
        DATA_PATH = r"/data/aibox-for-gdyidong/weights"
    else:
        print("not a supported platform")
        exit()

    # set transformer cache path
    MODEL_PATH = DATA_PATH
    XDG_CACHE_HOME = os.path.join(DATA_PATH, ".cache")

    # set transformer offline mode and cache path
    os.environ["TRANSFORMERS_OFFLINE"] = "1"
    os.environ["XDG_CACHE_HOME"] = XDG_CACHE_HOME

    def status(self, logger):
        logger.info("USE: " + self.__class__.__name__)
        for attr in dir(self):
            if not attr.startswith("__") and \
                    not callable(getattr(self, attr)) and \
                    attr not in ["LOGO", "SECRET_KEY"]:
                logger.info(f"{attr}: {getattr(self, attr)}")

    def __str__(self):
        text = "\n".join(
            [
                attr + ": " + str(getattr(self, attr))
                for attr in dir(self)
                if not attr.startswith("__") and
                not callable(getattr(self, attr)) and
                attr not in ["LOGO", "SECRET_KEY"]
            ]
        )
        return "\n" + text

    def __repr__(self):
        return self.__str__()


class Pro(Settings):
    ENV = 'PRO'


class Dev(Settings):
    ENV = 'DEV'


def get_settings():
    env = os.getenv('ENV', 'DEV')
    if env == 'PRO':
        return Pro()
    else:
        return Dev()


settings = get_settings()
