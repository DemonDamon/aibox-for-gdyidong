# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：utils
# @Date   ：2024/1/22 23:34
# @Author ：leemysw

# 2024/1/22 23:34   Create
# =====================================================
import os
import json
import time
import socket
import signal
import datetime
import functools
import itertools
import logging
import weakref
import traceback
import threading
import multiprocessing

from inspect import Signature, Parameter
from functools import wraps
from contextlib import contextmanager


ROOT_PATH = os.path.abspath(os.path.dirname(__file__)) + '/../'

def get_host():
    try:
        host = socket.gethostbyname(socket.gethostname())
        # On my system, this always gives me 127.0.0.1. Hence...
    except:
        host = ''
    if not host or host.startswith('127.') or host.startswith('172.26'):
        # ...the hard way.
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('4.2.2.1', 0))
        host = s.getsockname()[0]
    return host


def abspath(r_path):
    path = os.path.abspath(os.path.join(ROOT_PATH, r_path))
    sh_path = '/'.join(path.split('\\'))

    return sh_path


def sources_path(spath):
    path = os.path.abspath(os.path.join(ROOT_PATH, "resources"))
    for p in spath:
        path = os.path.join(path, p)

    sh_path = '/'.join(path.split('\\'))

    return sh_path


@contextmanager
def timeblock(label):
    start = time.time()
    try:
        yield
    finally:
        if time.time() - start != 0:
            print('{:15s} : {:5d} -- {:.6f}'.format(label, int(1 / (time.time() - start)), time.time() - start))


# ------------------------------------------------------------------- #
def runtime(func):
    @wraps(func)
    def wrap(*args, **kwargs):
        t = time.perf_counter()
        ret = func(*args, **kwargs)
        print('\n')
        print('-------------------------------')
        print('|', datetime.datetime.now())
        print('|', func.__name__, (time.perf_counter() - t), '|')
        print('|', func.__name__, 1 / (time.perf_counter() - t), '|')
        print('-------------------------------')
        return ret

    return wrap


def retry(delays=(0, 1, 5, 30, 180, 600, 3600)):
    def wrapper(func):
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            for delay in itertools.chain(delays, [None]):
                if delay is not None:
                    ret = func(*args, **kwargs)
                    success = int(ret.get('success', None))
                    if not success:
                        code = int(ret.get('code', None))
                        if code == 500:
                            return None
                        else:
                            time.sleep(delay)
                    else:
                        return ret

        return wrapped

    return wrapper


def error_handler(logger=logging):
    def wrapper(func):
        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except:
                msg = traceback.format_exc()
                logger.error(msg)
                return 'error'

        return wrapped

    return wrapper


def singleton(cls, *args, **kwargs):
    instances = {}

    def _singleton():
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]

    return _singleton


def ensure_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def shutdown_func(sig, frame):
    print('close')
    print('=' * 50)
    print('-' * 50)
    print('=' * 50)


def shutdown(func=shutdown_func):
    for sig in [signal.SIGQUIT, signal.SIGTERM, signal.SIGKILL]:
        signal.signal(sig, func)


def check_ip(ip):
    try:
        socket.inet_aton(ip)
        return True
    except socket.error:
        return False


def make_sig(*names):
    parms = [Parameter(name, Parameter.POSITIONAL_OR_KEYWORD) for name in names]
    return Signature(parms)


def synchronized(func):
    """
    Decorator in order to achieve thread-safe singleton class.
    """
    func.__lock__ = threading.Lock()

    def lock_func(*args, **kwargs):
        with func.__lock__:
            return func(*args, **kwargs)

    return lock_func


# pnpoly
def pnppoly(point, verts):
    """
    - PNPoly算法
    - point  (x, y)
    - xyverts  [(x1, y1), (x2, y2), (x3, y3), ...]
    """
    try:
        x, y = point
    except:
        return False
    vertx = [xyvert[0] for xyvert in verts]
    verty = [xyvert[1] for xyvert in verts]

    # N个点中，横坐标和纵坐标的最大值和最小值，判断目标坐标点是否在这个四边形之内
    if not verts or not min(vertx) <= x <= max(vertx) or not min(verty) <= y <= max(verty):
        return False

    # 上一步通过后，核心算法部分
    nvert = len(verts)
    is_in = False
    for i in range(nvert):
        j = nvert - 1 if i == 0 else i - 1
        if ((verty[i] > y) != (verty[j] > y)) and (
                x < (vertx[j] - vertx[i]) * (y - verty[i]) / (verty[j] - verty[i]) + vertx[i]):
            is_in = not is_in

    return is_in


# ===========================================================================================

# ========================== 单例模式 ==========================

class SingleInstanceMetaClass(type):
    __cache = weakref.WeakValueDictionary()

    def __init__(cls, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __call__(cls, *args, **kwargs):
        cache_key = cls.cache(**kwargs)
        if cache_key is None:
            cache_key = 'default'

        if cache_key in cls.__cache:
            return cls.__cache[cache_key]

        new_class = cls.__new__(cls)
        new_class.__init__(*args, **kwargs)
        cls.__cache[cache_key] = new_class
        return new_class

    @staticmethod
    def cache(**kwargs):
        key = kwargs.get('name', None)
        return key

    @synchronized
    def __new__(cls, *args, **kwargs):
        return super().__new__(cls, *args, **kwargs)


class Singleton(type):
    def __init__(self, *args, **kwargs):
        self.__instance = None
        super().__init__(*args, **kwargs)

    @synchronized
    def __call__(self, *args, **kwargs):
        if self.__instance is None:
            self.__instance = super(Singleton, self).__call__(*args, **kwargs)
        return self.__instance


# ========================== 单例模式 ==========================

class NoInstances(type):
    def __call__(self, *args, **kwargs):
        raise TypeError("can't instantiate directly")


class StructureMeta(type):
    def __new__(cls, clsname, bases, clsdict):
        clsdict['__signature__'] = make_sig(*clsdict.get('_fields', []))
        return super().__new__(cls, clsname, bases, clsdict)


class BaseStructure_s(metaclass=StructureMeta):
    _fields = []

    def __init__(self, *args, **kwargs):
        bound_values = self.__signature__.bind(*args, **kwargs)

        for name, values in bound_values.arguments.items():
            setattr(self, name, values)


class BaseStructure:
    _fields = []
    _default = []

    def __init__(self, *args, **kwargs):
        if len(args) > len(self._fields):
            raise TypeError('Expected {} arguments'.format(len(self._fields)))

        for key, value in zip(self._fields, args):
            setattr(self, key, value)

        extra_args = kwargs.keys() - self._fields
        for key in extra_args:
            setattr(self, key, kwargs.pop(key))

        if kwargs:
            raise TypeError('Duplicate values for {}'.format(','.join(kwargs)))


class MyThread():
    def __init__(self, func, thread_num, *args, **kwargs) -> None:
        self.func = func
        self.args = args
        self.kwargs = kwargs
        self.thread_num = thread_num

        self.threads = []

    def run(self):
        for t in range(self.thread_num):
            self.threads.append(threading.Thread(target=self.func, args=self.args, kwargs=self.kwargs))

        for t in self.threads:
            t.setDaemon(True)
            t.start()

        for t in self.threads:
            t.join()


class MyProcess():
    def __init__(self, func, thread_num, *args, **kwargs) -> None:
        self.func = func
        self.args = args
        self.kwargs = kwargs
        self.thread_num = thread_num

        self.threads = []

    def run(self):
        for t in range(self.thread_num):
            self.threads.append(multiprocessing.Process(target=self.func, args=self.args, kwargs=self.kwargs))

        for t in self.threads:
            t.daemon = True
            t.start()

        for t in self.threads:
            t.join()


class JsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(obj, datetime.date):
            return obj.strftime('%Y-%m-%d')
        else:
            return json.JSONEncoder.default(self, obj)
