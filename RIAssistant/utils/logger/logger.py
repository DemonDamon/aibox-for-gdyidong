# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：logger
# @Date   ：2024/1/22 23:27
# @Author ：leemysw

# 2024/1/22 23:27   Create
# =====================================================
import sys
import time
import logging
from typing import Optional
from logging import handlers
from logging.handlers import RotatingFileHandler, TimedRotatingFileHandler

from utils.utils import abspath


def setup_logger(
        name: str,
        save: Optional[bool] = False,
        filename: Optional[str] = None,
        mode: str = 'w',
        distributed_rank: bool = False,
        stdout: bool = True,
        socket: bool = False,
        rotating_size: bool = False,
        rotating_time: bool = False,
        level: str = 'info'
):
    """
    日志模块

    :param level: 日志级别
    :param name: 日志名称
    :param filename: 日志文件名
    :param mode: 写模式
    :param distributed_rank: 是否分布式
    :param stdout: 是否终端输出
    :param save: 是否保存日志文件
    :param socket: 是否输出到socket
    :param rotating_size: 是否按文件大小切割
    :param rotating_time: 是都按日期切割
    :return:
    """

    if name in logging.Logger.manager.loggerDict.keys():
        return logging.getLogger(name)

    logger = logging.getLogger(name)
    level = level.upper()
    logger.setLevel(level)
    logger.propagate = False
    if distributed_rank:
        return logger

    formatter = logging.Formatter(
        fmt="%(asctime)s -> %(levelname)-8s: %(module)-15s | %(lineno)-3d | %(message)s",
        # fmt="%(levelname)s:  %(asctime)22s - %(processName)-15s  %(module)-10s| %(lineno)-2d | %(message)s",
        # fmt="%(asctime)s %(processName)-15s %(levelname)-8s| %(module)-15s| %(funcName)-15s| %(lineno)-5d | %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S"
    )

    if stdout:
        ch = logging.StreamHandler(stream=sys.stdout)
        ch.setLevel(level)
        ch.setFormatter(formatter)
        logger.addHandler(ch)

    if socket:
        socketHandler = handlers.SocketHandler('localhost', logging.handlers.DEFAULT_TCP_LOGGING_PORT)
        socketHandler.setLevel(level)
        socketHandler.setFormatter(formatter)
        logger.addHandler(socketHandler)

    if save or filename:
        if filename is None:
            filename = time.strftime("%Y-%m-%d_%H.%M.%S", time.localtime()) + ".log"

        if rotating_time:
            # 每 1(interval) 天(when) 重写1个文件,保留7(backupCount) 个旧文件；when还可以是Y/m/H/M/S
            th = TimedRotatingFileHandler(filename, when='M', interval=1, backupCount=3, encoding="UTF-8")
            th.setLevel(level)
            th.setFormatter(formatter)
            logger.addHandler(th)

        if rotating_size:
            # 每 1024Bytes重写一个文件,保留2(backupCount) 个旧文件
            sh = RotatingFileHandler(filename, mode=mode, maxBytes=1024 * 1024, backupCount=5, encoding="UTF-8")
            sh.setLevel(level)
            sh.setFormatter(formatter)
            logger.addHandler(sh)

        else:
            fh = logging.FileHandler(filename, mode=mode, encoding="UTF-8")
            fh.setLevel(level)
            fh.setFormatter(formatter)
            logger.addHandler(fh)

    return logger


logger = setup_logger(
    name='main',
    filename=abspath('logs/main.log'),
    stdout=True,
)
