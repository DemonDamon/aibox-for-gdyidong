# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：register_init
# @Date   ：2024/1/22 16:11
# @Author ：leemysw

# 2024/1/22 16:11   Create
# =====================================================
from fastapi import FastAPI

# from database.get_db import get_db
from apps.server.common.base_logger import logger


def register_init(app: FastAPI) -> None:
    """
    初始化连接
    :param app:
    :return:
    """

    @app.on_event("startup")
    async def init_connect():
        # 连接redis
        # redis_client.init_redis_connect()
        # db = get_db()
        # redis = get_db('redis')

        # setattr(app, 'db', db)
        # setattr(app, 'redis', redis.con)
        setattr(app, 'logger', logger)
        ...

    @app.on_event('shutdown')
    async def shutdown_connect():
        """
        关闭
        :return:
        """
        ...
        # try:
        #     app.db.close()
        #     await app.redis.close()
        # except:
        #     ...
        # schedule.shutdown()
