# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：register_exception
# @Date   ：2024/1/22 16:14
# @Author ：leemysw

# 2024/1/22 16:14   Create
# =====================================================

import traceback

from fastapi import FastAPI, Request
from fastapi.exceptions import RequestValidationError, ResponseValidationError

from apps.server.common import base_resp as resp
from apps.server.common.base_logger import logger


class TokenAuthError(Exception):
    def __init__(self, err_desc: str = "User Authentication Failed"):
        self.err_desc = err_desc


class TokenExpired(Exception):
    def __init__(self, err_desc: str = "Token has expired"):
        self.err_desc = err_desc


class AuthenticationError(Exception):
    def __init__(self, err_desc: str = "Permission denied"):
        self.err_desc = err_desc


def register_exception(app: FastAPI) -> None:
    """
    捕获异常
    :param app:
    :return:
    """

    # 自定义异常 捕获
    @app.exception_handler(TokenExpired)
    async def user_not_found_exception_handler(request: Request, exc: TokenExpired):
        """
        token过期
        :param request:
        :param exc:
        :return:
        """
        logger.error(
            f"token未知用户\nURL:{request.method}{request.url}\nHeaders:{request.headers}\n{traceback.format_exc()}")

        return resp.fail(resp.DataNotFound.set_msg(exc.err_desc))

    @app.exception_handler(TokenAuthError)
    async def user_token_exception_handler(request: Request, exc: TokenAuthError):
        """
        用户token异常
        :param request:
        :param exc:
        :return:
        """
        logger.error(f"用户认证异常\nURL:{request.method}{request.url}\nHeaders:{request.headers}\n{traceback.format_exc()}")

        return resp.fail(resp.DataNotFound.set_msg(exc.err_desc))

    @app.exception_handler(AuthenticationError)
    async def user_not_found_exception_handler(request: Request, exc: AuthenticationError):
        """
        用户权限不足
        :param request:
        :param exc:
        :return:
        """
        logger.error(f"用户权限不足 \nURL:{request.method}{request.url}")
        return resp.fail(resp.PermissionDenied)

    @app.exception_handler(ResponseValidationError)
    async def inner_validation_exception_handler(request: Request, exc: ResponseValidationError):
        """
        内部参数验证异常
        :param request:
        :param exc:
        :return:
        """
        logger.error(
            f"内部参数验证错误\nURL:{request.method}{request.url}\nHeaders:{request.headers}\n{traceback.format_exc()}")
        return resp.fail(resp.BusinessError.set_msg(exc.errors()))

    @app.exception_handler(RequestValidationError)
    async def request_validation_exception_handler(request: Request, exc: RequestValidationError):
        """
        请求参数验证异常
        :param request:
        :param exc:
        :return:
        """
        logger.error(
            f"请求参数格式错误\nURL:{request.method}{request.url}\nHeaders:{request.headers}\n{traceback.format_exc()}")
        # return response_code.resp_4001(message='; '.join([f"{e['loc'][1]}: {e['msg']}" for e in exc.errors()]))
        return resp.fail(resp.InvalidParams.set_msg(exc.errors()))

    # 捕获全部异常
    @app.exception_handler(Exception)
    async def all_exception_handler(request: Request, exc: Exception):
        """
        全局所有异常
        :param request:
        :param exc:
        :return:
        """
        logger.error(f"全局异常\n{request.method}URL:{request.url}\nHeaders:{request.headers}\n{traceback.format_exc()}")
        return resp.fail(resp.ServerError)
