# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：register_static
# @Date   ：2024/1/22 16:18
# @Author ：leemysw

# 2024/1/22 16:18   Create
# =====================================================
import os

from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles


def register_static_file(app: FastAPI) -> None:
    """
    :param app:
    :return:
    """

    if not os.path.exists("./static"):
        os.mkdir("./static")
    app.mount("/static", StaticFiles(directory="static"), name="static")
