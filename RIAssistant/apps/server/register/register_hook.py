# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：register_hoook
# @Date   ：2024/1/22 16:13
# @Author ：leemysw

# 2024/1/22 16:13   Create
# =====================================================

import time

from fastapi import FastAPI, Request, Response

from apps.server.common.base_logger import logger

def register_hook(app: FastAPI) -> None:
    """
    请求响应拦截 hook
    https://fastapi.tiangolo.com/tutorial/middleware/
    :param app:
    :return:
    """

    # @app.middleware("http")
    # async def logger_request(request: Request, call_next) -> Response:
    #     # https://stackoverflow.com/questions/60098005/fastapi-starlette-get-client-real-ip
    #     # logger.info(f"访问记录:{request.method} url:{request.url}\nheaders:{request.headers}\nIP:{request.client.host}")
    #     setattr(request, "logger", logger)
    #     response = await call_next(request)
    #     return response

    @app.middleware("http")
    async def add_process_time_header(request: Request, call_next):
        start_time = time.time()
        response = await call_next(request)
        process_time = time.time() - start_time
        logger.info(f"process_time:{process_time}")
        response.headers["X-Process-Time"] = str(process_time)
        return response

