# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：register_middleware
# @Date   ：2024/1/22 16:16
# @Author ：leemysw

# 2024/1/22 16:16   Create
# =====================================================

from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from fastapi.middleware.gzip import GZipMiddleware

from cores.config import settings
# from database.get_db import get_db
# from apps.server.common.security import get_casbin
# from apps.server.common.middleware import AuthenticationMiddleware
#

def register_middleware(app: FastAPI) -> None:
    """
    支持跨域
    :param app:
    :return:
    """
    # if settings.BACKEND_CORS_ORIGINS:

    # db = get_db()
    # app.add_middleware(
    #     AuthenticationMiddleware,
    #     enforcer=get_casbin(db.engine)
    # )

    app.add_middleware(
        CORSMiddleware,
        allow_origins=[
            str(origin) for origin in settings.BACKEND_CORS_ORIGINS
        ],  # 设置允许的origins来源
        allow_credentials=True,
        # 设置允许跨域的http方法，比如 get、post、put等。
        allow_methods=["*"],
        # 允许跨域的headers，可以用来鉴别来源等作用。
        allow_headers=["*"]
    )

    app.add_middleware(
        GZipMiddleware,
        minimum_size=500
    )
