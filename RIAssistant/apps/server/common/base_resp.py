# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：base_resp
# @Date   ：2024/1/22 23:24
# @Author ：leemysw

# 2024/1/22 23:24   Create
# =====================================================
from typing import Union

from fastapi import status as http_status
from fastapi.responses import JSONResponse, Response
from fastapi.encoders import jsonable_encoder


class Resp(object):
    def __init__(self, status: int, msg: str, code: int = http_status.HTTP_400_BAD_REQUEST):
        self.status = status
        self.msg = msg
        self.code = code

    def set_msg(self, msg):
        self.msg = msg
        return self


PermissionDenied: Resp = Resp(401, "权限拒绝", http_status.HTTP_401_UNAUTHORIZED)
InvalidParams: Resp = Resp(402, "无效的参数", http_status.HTTP_400_BAD_REQUEST)
BusinessError: Resp = Resp(403, "业务错误", http_status.HTTP_403_FORBIDDEN)
DataNotFound: Resp = Resp(404, "查询失败", http_status.HTTP_400_BAD_REQUEST)
DataStoreFail: Resp = Resp(405, "新增失败", http_status.HTTP_400_BAD_REQUEST)
DataUpdateFail: Resp = Resp(406, "更新失败", http_status.HTTP_400_BAD_REQUEST)
DataDestroyFail: Resp = Resp(407, "删除失败", http_status.HTTP_400_BAD_REQUEST)
ServerError: Resp = Resp(500, "服务器繁忙", http_status.HTTP_500_INTERNAL_SERVER_ERROR)


def ok(*, data: Union[list, dict, str] = None, pagination: dict = None, msg: str = "success") -> Response:
    return JSONResponse(
        status_code=http_status.HTTP_200_OK,
        content=jsonable_encoder({
            'status': 200,
            'msg': msg,
            'data': data,
            'pagination': pagination
        })
    )


def fail(response: Resp) -> Response:
    return JSONResponse(
        status_code=response.code,
        content=jsonable_encoder({
            'status': response.status,
            'msg': response.msg,
        })
    )
