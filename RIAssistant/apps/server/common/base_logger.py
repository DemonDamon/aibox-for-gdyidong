# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：base_logger
# @Date   ：2024/1/22 23:27
# @Author ：leemysw

# 2024/1/22 23:27   Create
# =====================================================
from utils.logger import setup_logger
from utils.utils import abspath

logger = setup_logger(
    name='server',
    filename=abspath('logs/server.log'),
    stdout=True,
)


logger.info("初始化")
