# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：session
# @Date   ：2024/1/23 11:29
# @Author ：leemysw

# 2024/1/23 11:29   Create
# =====================================================
from collections import defaultdict

from .model import Message


class MemoryInterface:
    def append(self, session_id: str, message: Message) -> None:
        pass

    def get(self, session_id: str) -> list:
        pass

    def remove(self, session_id: str) -> list:
        pass


class Memory(MemoryInterface):
    def __init__(self):
        self.storage = defaultdict(list)

    def initialize(self, session_id: str):
        self.storage[session_id] = []

    def append(self, session_id: str, message: Message) -> list:
        if not self.storage[session_id]:
            self.initialize(session_id)
        self.storage[session_id].append(message)
        return self.storage[session_id]

    def get(self, session_id: str) -> list:
        return self.storage[session_id]

    def remove(self, session_id: str) -> None:
        self.storage[session_id] = []


session = Memory()
