# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：__init__
# @Date   ：2024/1/23 0:45
# @Author ：leemysw

# 2024/1/23 0:45   Create
# =====================================================
from fastapi import APIRouter

from . import api_assistant

router_assistant = APIRouter()
router_assistant.include_router(api_assistant.router)
