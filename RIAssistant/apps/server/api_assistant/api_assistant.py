# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：api_assistant
# @Date   ：2024/1/22 16:34
# @Author ：leemysw

# 2024/1/22 16:34   Create
# =====================================================

from fastapi import APIRouter

from apps.server.common import cbv, resp

from .session import session
from .server import AssistantServer
from .model import Question, Answer, Message, Document, Session


router = APIRouter()


@cbv(router)
class ApiAssistant:

    @router.post("/chat", response_model=Answer)
    def chat(self, question: Question):
        """
        chat with assistant
        """

        documents = AssistantServer.retrieve(question)
        new_answer = AssistantServer.generate_answer(question, documents)
        message = Message(
                question=question.question,
                answer=new_answer,
                documents=documents
            )

        session.append(question.session_id, message)

        answer = Answer(
            session_id=question.session_id,
            question=question.question,
            answer=new_answer,
            history=session.get(question.session_id),
            documents=documents
        )

        return resp.ok(data=answer.model_dump())

    @router.get("/query", response_model=Session)
    def query(self, session_id: str):
        """
        query session
        """
        session_data = Session(
            session_id=session_id,
            history=session.get(session_id)
        )

        return resp.ok(data=session_data.model_dump())
