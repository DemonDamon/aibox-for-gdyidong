# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：server
# @Date   ：2024/1/24 09:49
# @Author ：leemysw

# 2024/1/24 09:49   Create
# =====================================================
import json
from typing import List, Union

import requests

from apps.rag import rag

from .model import Document, GlmConfig, Question
from .session import session

glm_config = GlmConfig(
    prompt='''你是一个资深的中国移动营销助手，擅长利用你的知识库来回答有关中国移动业务的相关问题。
            你会涉及到的任务主要是以下几个：
            1.根据客户的咨询运用特定的话术回答客户问题并且推荐相关的公司套餐或服务
            2.根据客户的需求，运用特定的话术指导客户进行相关操作。
            客户的问题或者咨询会包裹在<>两个尖括号内，如："<客户的问题>"；\
            你的知识库相关内容会使用||两个竖线进行包裹，如："|我的知识库内容|"。\
            在有多轮对话过程中，你只需要根据本轮对话所搭载的知识库内容来进行回答。
            你需要两个步骤来完成你的任务，
            步骤一：根据客户的问题以及客户的历史问题判断客户需求以及意图。
            步骤二：根据步骤一的结果，依据知识库的话术和内容进行生成对应的回答，如果知识库为空或者没有
            和客户问题相关的内容，生成“我不清楚这个问题。”\
            最后返回生成的回答。''',
    history=[],
    do_sample=False,
    presence_penalty=0.0,
    frequency_penalty=0.0,
    temperature=0.8,
    top_p=0.8,
    ignore_eos=False,
    max_new_tokens=2048
)


class MsgTemplate():
    def __init__(self, config: GlmConfig):
        self.msg = {**(config.model_dump())}

    def __call__(self, query, doc):
        self.msg['history'] = [query[:-1]]
        self.msg["history"][0].append(f'<{query[-1]}>，|{doc}|')
        print(self.msg)
        return self.msg


class AssistantServer:
    llm = MsgTemplate(glm_config)

    @classmethod
    def retrieve(cls, question: Question):
        resp = rag(question.question)
        documents = []
        for i in resp:
            document = Document(
                content=i.page_content,
                retrieval_query=question.question,
                file_id=str(i.metadata.get("file_id")),
                file_name=i.metadata.get("file_name"),
                score=str(i.metadata.get("relevance_score")),
            )
            documents.append(document)
        return documents

    @classmethod
    def generate_answer(cls, question: Question, documents: List[Document]):
        history = session.get(question.session_id)
        history_list = []
        for doc in history:
            history_list.append(doc.question)
            history_list.append(doc.answer)

        history_list.append(question.question)

        anwser = cls.generated(history_list, documents[0].content)
        # anwser = "Sorry, I don't have an answer for this question."

        return anwser

    @classmethod
    def generated(cls, query, document: Union[str, None] = None):
        URL = 'http://192.168.32.115:8000/aibox/chatLLM/v1.0/generate'
        headers = {
            'User-Agent': 'Apifox/1.0.0 (https://apifox.com)',
            'Content-Type': 'application/json',
            'Accept': '*/*',
            'Host': '192.168.32.115:8000',
            'Connection': 'keep-alive'
        }
        cls.llm(query, document)
        payload = json.dumps(cls.llm.msg)
        response = requests.request('POST', url=URL, headers=headers, data=payload)
        print(response)
        # resp = response.json()
        return response.text
