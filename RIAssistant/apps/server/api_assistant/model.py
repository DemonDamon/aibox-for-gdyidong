# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：model
# @Date   ：2024/1/23 09:32
# @Author ：leemysw

# 2024/1/23 09:32   Create
# =====================================================

from typing import List, Union
from pydantic import BaseModel


class Document(BaseModel):
    """
    文本内容
    """
    file_id: str  # 文本内容对应的文件id
    file_name: str  # 文本内容对应的文件名
    content: str  # 文本内容
    retrieval_query: str  # 文本内容对应的检索问题
    score: str  # 检索结果的相关性得分，分数越高越相关


class Message(BaseModel):
    """
    历史回答
    """
    question: str
    answer: str
    documents: Union[None, List[Document]] = None


class Question(BaseModel):
    """
    问题
    """
    session_id: str  # 问答会话id
    question: str  # 问题
    history: Union[None, List[Message]] = None  # 历史回答


class Session(BaseModel):
    """
    问答会话
    """
    session_id: str  # 问答会话id
    history: Union[None, List[Message]] = None  # 历史回答


class Answer(Session):
    """
    答案
    """
    question: str
    answer: str
    documents: List[Document]


class GlmConfig(BaseModel):
    prompt: str = ''
    history: List[Union[None, List[str]]] = []
    do_sample: bool = False
    presence_penalty: float = 0.0
    frequency_penalty: float = 0.0
    temperature: float = 0.8
    top_p: float = 0.8
    ignore_eos: bool = False
    max_new_tokens: int = 2048