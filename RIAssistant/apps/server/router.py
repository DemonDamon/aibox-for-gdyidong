# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：router
# @Date   ：2024/1/22 23:22
# @Author ：leemysw

# 2024/1/22 23:22   Create
# =====================================================
from fastapi import APIRouter

from .api_assistant import router_assistant

api_router = APIRouter()
api_router.include_router(
    router_assistant
)
