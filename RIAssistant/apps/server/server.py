# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：server
# @Date   ：2024/1/23 0:22
# @Author ：leemysw

# 2024/1/23 0:22   Create
# =====================================================
from fastapi import FastAPI

from cores.config import settings

from .router import api_router
from .register import *


def create_app() -> FastAPI:
    """
    生成FatAPI对象
    :return:
    """
    app = FastAPI(
        debug=True,
        title=settings.PROJECT_NAME,
        openapi_url=f"{settings.API_V1_STR}/openapi.json",
    )
    # 取消挂载在 request对象上面的操作，感觉特别麻烦，直接使用全局的
    register_init(app)

    # 注册中间件
    register_middleware(app)

    # 注册路由
    register_router(app)

    # 注册捕获全局异常
    register_exception(app)

    # 请求拦截
    register_hook(app)

    # 分页
    # register_pagination(app)

    return app


def register_router(app: FastAPI) -> None:
    """
    注册路由
    :param app:
    :return:
    """
    # 项目API
    app.include_router(
        api_router,
    )
