# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：__init__
# @Date   ：2024/1/23 22:32
# @Author ：leemysw

# 2024/1/23 22:32   Create
# =====================================================
import os
from cores.config import settings
from langchain_community.embeddings import HuggingFaceEmbeddings

from .bce_rerank import BCERerank

__all__ = ['embedding', 'rerank']

# init embedding model
embedding_model_name = os.path.join(settings.DATA_PATH, "bce-embedding-base_v1")
embedding_model_kwargs = {'device': settings.DEVICE}
embedding_encode_kwargs = {'batch_size': 32, 'normalize_embeddings': True, 'show_progress_bar': False}

embedding = HuggingFaceEmbeddings(
    model_name=embedding_model_name,
    model_kwargs=embedding_model_kwargs,
    encode_kwargs=embedding_encode_kwargs
)

# init rerank model
rerank_model_name = os.path.join(settings.DATA_PATH, "bce-reranker-base_v1")
rerank_args = {'model': rerank_model_name, 'top_n': 5, 'device': settings.DEVICE}
rerank = BCERerank(**rerank_args)
