# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：rag
# @Date   ：2024/1/23 14:19
# @Author ：leemysw

# 2024/1/23 14:19   Create
# =====================================================
from typing import List

from langchain_core.documents import Document
from langchain_community.vectorstores import FAISS
from langchain_community.vectorstores.utils import DistanceStrategy
from langchain.retrievers import ContextualCompressionRetriever

from .bce import embedding, rerank
from .tools.load_file import LoadFile

from utils.logger import logger


class RAG:
    def __init__(self, files):
        self.embedding = embedding
        self.rerank = rerank
        self.knowledge = LoadFile(files=files, logger=logger)

        self.retriever = FAISS.from_documents(
            documents=self.knowledge.docs,
            embedding=self.embedding,
            distance_strategy=DistanceStrategy.MAX_INNER_PRODUCT
        ).as_retriever(
            search_type="similarity",
            search_kwargs={"score_threshold": 0.3, "k": 10}
        )

        self.compressor = ContextualCompressionRetriever(
            base_compressor=self.rerank, base_retriever=self.retriever
        )

    def __call__(self, query, *args, **kwargs, ) -> List[Document]:
        response: List[Document] = self.compressor.get_relevant_documents(query)
        return response
