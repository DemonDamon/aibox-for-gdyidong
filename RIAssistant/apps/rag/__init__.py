# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：__init__.py
# @Date   ：2024/1/23 14:08
# @Author ：leemysw

# 2024/1/23 14:08   Create
# =====================================================
from pathlib import Path

from cores.config import settings

from .rag import RAG

__all__ = ['rag']

path = Path(settings.FILEPATH)
file_list = []
for file in path.glob("*.docx"):
    file_list.append(file.as_posix())

rag = RAG(file_list)
