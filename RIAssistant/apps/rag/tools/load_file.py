# !/usr/bin/env python
# -*- coding: utf-8 -*-
# =====================================================
# @File   ：load_file
# @Date   ：2024/1/23 14:37
# @Author ：leemysw

# 2024/1/23 14:37   Create
# =====================================================

import os
import logging

from typing import List

from langchain.docstore.document import Document
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.document_loaders import UnstructuredWordDocumentLoader

from .chinese_title_enhance import zh_title_enhance


class LoadFile:
    def __init__(self, files: List[str], logger=logging):
        self.logger = logger

        self.files = files
        self.docs: List[Document] = []
        self.load_file()
        self.logger.info(f"all file load success!")

    def load_file(self):
        """
        load file to Document object
        """
        if not self.files:
            self.logger.error("no file to load!")

        elif isinstance(self.files, list):
            for idx, file in enumerate(self.files):
                if os.path.isfile(file):
                    file_name = os.path.basename(file)
                    try:
                        self.split_file_to_docs(idx, file, using_zh_title_enhance=True)
                        self.logger.info(f'success init localfile {file_name}')
                    except:
                        self.logger.exception(f"file {file} load error!")
                else:
                    self.logger.error(f"file {file} not exists!")

    def split_file_to_docs(self, file_id, file_path, using_zh_title_enhance=False):
        """
        split file to Document object
        """
        if file_path.lower().endswith(".docx"):
            loader = UnstructuredWordDocumentLoader(file_path, mode="elements")
            docs = loader.load()
        else:
            raise TypeError("文件类型不支持，目前仅支持：[docx]")

        if using_zh_title_enhance:
            self.logger.info("using_zh_title_enhance %s", using_zh_title_enhance)
            docs = zh_title_enhance(docs)

        # 重构docs，如果doc的文本长度大于800tokens，则利用text_splitter将其拆分成多个doc
        # text_splitter: RecursiveCharacterTextSplitter
        text_splitter = RecursiveCharacterTextSplitter(
            chunk_size=1500,
            chunk_overlap=200
            # length_function=100,
        )
        docs = text_splitter.split_documents(docs)

        # 这里给每个docs片段的metadata里注入file_id
        for doc in docs:
            doc.metadata["file_id"] = file_id
            doc.metadata["file_name"] = os.path.split(file_path)[-1]
        self.write_check_file(file_path, docs)
        if docs:
            self.logger.info('langchain analysis content success!')
        else:
            self.logger.info('langchain analysis docs is empty!')
        self.docs.extend(docs)

    @staticmethod
    def write_check_file(filepath, docs):
        """
        write check file for debug
        """
        folder_path = os.path.join(os.path.dirname(filepath), "tmp_files")
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)
        fp = os.path.join(folder_path, 'load_file.txt')
        with open(fp, 'w', encoding='utf-8') as fout:
            fout.write("filepath=%s,len=%s" % (filepath, len(docs)))
            fout.write('\n')
            for i in docs:
                fout.write(str(i))
                fout.write('\n')
            fout.close()
